package br.org.emilio.newage3.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Component;

import br.org.emilio.newage3.service.MyUserDetailsService;

@Component
public class AuthTokenProvider {

	@Autowired
	private JwtDecoder jwtDecoder;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	
	public JwtDecoder getJwtDecoder() {
		return this.jwtDecoder;
	}
	
	
	public MyUserDetailsService getUserDetailsService() {
		return this.userDetailsService;
	}
	
}
