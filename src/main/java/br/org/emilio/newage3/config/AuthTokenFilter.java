package br.org.emilio.newage3.config;

import java.io.IOException;
import java.util.Date;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import br.org.emilio.newage3.dto.MyUserDetails;
import br.org.emilio.newage3.service.MyUserDetailsService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class AuthTokenFilter extends OncePerRequestFilter {

	private MyUserDetailsService userDetailsService;
	private JwtDecoder jwtDecoder;
	
	public AuthTokenFilter(AuthTokenProvider authTokenProvider) {
		this.userDetailsService = authTokenProvider.getUserDetailsService();
		this.jwtDecoder = authTokenProvider.getJwtDecoder();
	}
	
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		
		String token = this.getToken(request);
		
		if (token != null) {
			Jwt jwt = this.jwtDecoder.decode(token);
			long now = new Date().getTime();
			long begin = Date.from(jwt.getNotBefore()).getTime();
			long end = Date.from(jwt.getExpiresAt()).getTime();
			
			if (now >= begin && now <= end) {
				String username = jwt.getSubject();
	
				MyUserDetails userDetails = (MyUserDetails) 
						this.userDetailsService.loadUserByUsername(username);
				userDetails.setPassword(null);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
	
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}

		filterChain.doFilter(request, response);
	}
	
	
	private String getToken(HttpServletRequest request) {
		String headerAuth = request.getHeader("Authorization");

		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
			return headerAuth.substring(7, headerAuth.length());
		}

		return null;
	}

}
