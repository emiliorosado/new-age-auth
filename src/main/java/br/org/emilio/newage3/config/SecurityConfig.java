package br.org.emilio.newage3.config;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jose.jws.MacAlgorithm;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.nimbusds.jose.jwk.source.ImmutableSecret;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;

@Configuration
public class SecurityConfig {
	
	@Value("${jwt.secret.key}")
	private String secretKey;

	
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, AuthTokenProvider authTokenProvider) throws Exception {
        http
			.cors()
			.and()
			.csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeHttpRequests(authorize -> authorize
				.requestMatchers("/login").permitAll()
                .anyRequest().authenticated()
			)
        	.addFilterBefore(new AuthTokenFilter(authTokenProvider), UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	
	@Bean
	public JwtDecoder jwtDecoder() {
		return NimbusJwtDecoder
			.withSecretKey(this.getSecretKey(this.secretKey))
			.macAlgorithm(MacAlgorithm.HS256)
			.build();
	}

	
	@Bean
	public JwtEncoder jwtEncoder() {
		JWKSource<SecurityContext> immutableSecret = 
			new ImmutableSecret<SecurityContext>(this.getSecretKey(this.secretKey));
		return new NimbusJwtEncoder(immutableSecret);
	}
	
	
	private SecretKey getSecretKey(String secretKey) {
		Date date = new Date();
		String delimit1 = new SimpleDateFormat("ddMM").format(date);
		String delimit2 = new SimpleDateFormat("MMdd").format(date);
		
		byte[] key = (delimit1 + secretKey + delimit2).getBytes();
		key = key.length < 128 ? Arrays.copyOf(key, 128) : key;
		return new SecretKeySpec(key, "HMACSHA256");
	}
	
}
