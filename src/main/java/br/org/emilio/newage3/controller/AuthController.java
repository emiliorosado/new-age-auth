package br.org.emilio.newage3.controller;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.org.emilio.newage3.service.AuthService;

@RestController
public class AuthController {

	@Autowired
	private AuthService service;
	
	
	@PostMapping("/login")
	public ResponseEntity<Map<String, String>> login(@RequestHeader("Authorization") String headerAuth) {
		Map<String, String> map = this.service.login(headerAuth);
		
		if (map == null) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		
		return new ResponseEntity<Map<String, String>>(map, HttpStatus.OK);
	}
	
	
	@GetMapping("/user")
	public UserDetails user(Principal principal) {
		return this.service.user(principal);
	}
	
}
