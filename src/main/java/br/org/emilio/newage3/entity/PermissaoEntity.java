package br.org.emilio.newage3.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PermissaoEntity {

	private Long codigoUsuario;
	private String nome;

}
