package br.org.emilio.newage3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewAgeAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewAgeAuthApplication.class, args);
    }
    
}
