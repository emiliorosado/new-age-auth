package br.org.emilio.newage3.service;

import java.security.Principal;
import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwsHeader;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.org.emilio.newage3.dto.MyUserDetails;

@Service
public class AuthService {

	@Autowired
	private MyUserDetailsService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private JwtEncoder jwtEncoder;
	
	@Value("${jwt.token.valid.hour}")
	private int tokenValidHour;

	
	public Map<String, String> login(String headerAuth) {
		if (!StringUtils.hasText(headerAuth) || !headerAuth.startsWith("Basic ")) {
			return null;
		}
		
		String loginEncode = headerAuth.substring(6, headerAuth.length());
		byte[] loginDecode = Base64.getDecoder().decode(loginEncode);
		String[] login = new String(loginDecode).split(":");
		String username = login[0];
		String password = login[1];
		
		UserDetails userDetails;
		try {
			userDetails = this.userService.loadUserByUsername(username);
		} catch (UsernameNotFoundException e) {
			return null;
		}
		
		if (!this.passwordEncoder.matches(password, userDetails.getPassword())) {
			return null;
		}
		
		JwtClaimsSet claims = JwtClaimsSet.builder()
			.subject(username)
			.claim("username", username)
			.notBefore(Instant.now())
			.expiresAt(Instant.now().plusSeconds(60 * 60 * this.tokenValidHour))
			.build();
			
		JwsHeader jwsHeader = JwsHeader.with(() -> "HS256").build();
			
		String token = this.jwtEncoder.encode(
			JwtEncoderParameters.from(jwsHeader, claims)).getTokenValue();
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("token", token);
		
		return map;
	}
	
	
	public UserDetails user(Principal principal) {
		MyUserDetails userDetails;
		try {
			userDetails = (MyUserDetails) this.userService.loadUserByUsername(principal.getName());
			userDetails.setPassword(null);
		} catch (UsernameNotFoundException e) {
			userDetails = null;
		}

		return userDetails;
	}

}
