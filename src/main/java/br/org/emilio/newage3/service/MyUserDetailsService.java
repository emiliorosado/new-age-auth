package br.org.emilio.newage3.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.org.emilio.newage3.dto.MyUserDetails;
import br.org.emilio.newage3.entity.PermissaoEntity;
import br.org.emilio.newage3.entity.UsuarioEntity;
import br.org.emilio.newage3.repository.PermissaoRepository;
import br.org.emilio.newage3.repository.UsuarioRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PermissaoRepository permissaoRepository;

	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UsuarioEntity entity = this.usuarioRepository.getByNome(username);
		
		if (entity == null) {
			throw new UsernameNotFoundException("Usuario nao encontrado");
		}
		
		MyUserDetails userDetails = new MyUserDetails();
		userDetails.setUsername(entity.getNome());
		userDetails.setPassword(entity.getSenha());
		userDetails.setAuthorities(this.getAuthorities(entity.getCodigo()));
		userDetails.setAccountNonExpired(true);
		userDetails.setAccountNonLocked(true);
		userDetails.setCredentialsNonExpired(true);
		userDetails.setEnabled(true);
		
		return userDetails;
	}
	
	
	private Collection<? extends GrantedAuthority> getAuthorities(long codigoUsuario) {
		List<PermissaoEntity> permissoes = this.permissaoRepository.getByCodigoUsuario(codigoUsuario);
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		if (permissoes != null) {
			for (PermissaoEntity permissao : permissoes) {
				authorities.add(new SimpleGrantedAuthority(permissao.getNome())); 
			}
		}
		
		return authorities;
	}

}
