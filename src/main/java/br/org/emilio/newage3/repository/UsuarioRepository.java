package br.org.emilio.newage3.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import br.org.emilio.newage3.entity.UsuarioEntity;

@Repository
public class UsuarioRepository {
	
	@Autowired
	private PasswordEncoder passEncoder;
	
	private List<UsuarioEntity> listaUsuarios = null;
	
	
	private List<UsuarioEntity> carregarUsuarios() {
		String[] usuarios = {"emilio", "cezar", "rosado"};
		String[] senhas = {"123", "123", "123"};
		
		List<UsuarioEntity> list = new ArrayList<UsuarioEntity>();
		for ( int i = 0 ; i < usuarios.length ; i++ ) {
			UsuarioEntity entity = new UsuarioEntity();
			entity.setCodigo((long) i + 1);
			entity.setNome(usuarios[i]);
			entity.setSenha(this.passEncoder.encode(senhas[i]));
			list.add(entity);
		}
		
		return list;
	}
	
	
	public UsuarioEntity getByNome(String nome) {
		if (this.listaUsuarios == null) {
			this.listaUsuarios = this.carregarUsuarios();
		}
		
		for (UsuarioEntity entity : this.listaUsuarios) {
			if (nome.equals(entity.getNome())) {
				return entity;
			}
		}
		
		return null;
	}

}
