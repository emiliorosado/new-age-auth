package br.org.emilio.newage3.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.org.emilio.newage3.entity.PermissaoEntity;

@Repository
public class PermissaoRepository {
	
	private List<PermissaoEntity> listaPermissoes = this.carregarPermissoes();
	
	
	private List<PermissaoEntity> carregarPermissoes() {
		long[] usuarios = {1, 1, 2, 3};
		String[] permissoes = {"CLIENTE", "CLIENTE_WRITE", "CLIENTE", "XPTO"};
		
		List<PermissaoEntity> list = new ArrayList<PermissaoEntity>();
		for ( int i = 0 ; i < usuarios.length ; i++ ) {
			PermissaoEntity entity = new PermissaoEntity();
			entity.setCodigoUsuario(usuarios[i]);
			entity.setNome(permissoes[i]);
			list.add(entity);
		}
		
		return list;
	}
	
	
	public List<PermissaoEntity> getByCodigoUsuario(long codigoUsuario) {
		List<PermissaoEntity> list = new ArrayList<PermissaoEntity>();
		
		for (PermissaoEntity entity : listaPermissoes) {
			if (codigoUsuario == entity.getCodigoUsuario()) {
				list.add(entity);
			}
		}
		
		return list;
	}

}
